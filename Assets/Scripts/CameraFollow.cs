﻿using UnityEngine;

public class CameraFollow : MonoBehaviour
{

    [SerializeField] Transform player; // Transform pemain yang akan diikuti oleh kamera
    Vector3 offset; // Jarak relatif antara kamera dan pemain

    private void Start()
    {
        offset = transform.position - player.position; // Menghitung offset awal antara kamera dan pemain saat permainan dimulai
    }

    private void Update()
    {
        Vector3 targetPos = player.position + offset; // Menghitung posisi target yang harus diikuti oleh kamera
        targetPos.x = 0; // Memastikan kamera tetap berada pada sumbu x yang sama

        // Mengatur posisi kamera agar mengikuti posisi target
        transform.position = targetPos;
    }
}
