﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Coin : MonoBehaviour
{

    [SerializeField] float turnSpeed = 90f; // Kecepatan rotasi koin

    private void OnTriggerEnter(Collider other)
    {
        // Memeriksa jika objek yang bertabrakan adalah rintangan, jika ya, hancurkan koin
        if (other.gameObject.GetComponent<Obstacle>() != null)
        {
            Destroy(gameObject);
            return;
        }

        // Memeriksa apakah objek yang bertabrakan adalah pemain
        if (other.gameObject.name != "Player")
        {
            return; // Jika bukan pemain, koin tidak berinteraksi
        }

        // Menambahkan skor pemain
        GameManager.inst.IncrementScore();

        // Menghancurkan objek koin
        Destroy(gameObject);
    }

    private void Start()
    {
        // Metode Start tidak melakukan apa-apa pada koin ini
    }

    private void Update()
    {
        // Mengatur rotasi koin agar berputar dengan kecepatan tertentu
        transform.Rotate(0, 0, turnSpeed * Time.deltaTime);
    }
}
