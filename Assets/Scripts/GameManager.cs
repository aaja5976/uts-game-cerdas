﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class GameManager : MonoBehaviour
{

    int score; // Skor pemain
    public static GameManager inst; // Instance singleton dari GameManager

    [SerializeField] Text scoreText; // Referensi ke teks yang menampilkan skor pada UI

    [SerializeField] PlayerMovement playerMovement; // Referensi ke komponen PlayerMovement

    public void IncrementScore()
    {
        // Menambahkan skor pemain
        score++;
        // Mengupdate teks skor pada UI
        scoreText.text = "SCORE: " + score;
        // Meningkatkan kecepatan pemain
        playerMovement.speed += playerMovement.speedIncreasePerPoint;
    }

    private void Awake()
    {
        // Membuat instance singleton GameManager
        inst = this;
    }

    private void Start()
    {
        // Metode Start tidak melakukan apa-apa pada GameManager ini saat permainan dimulai
    }

    private void Update()
    {
        // Metode Update tidak melakukan apa-apa pada GameManager ini selama permainan
    }
}
